class SearchResultModel {
  String name;
  String url;

  SearchResultModel({
    required this.name,
    required this.url,
  });
}

class IndexModel {
  String name;
  String url;

  IndexModel({
    required this.name,
    required this.url,
  });
}
