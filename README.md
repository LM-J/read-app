# 小说爬取App

@LM  2021-5-1 至今

## 介绍

由`Flutter+Dart`实现的网络小说爬取App，拥有简单的搜索，阅读功能，能对阅读历史进行记录。

```shell
# 打包
flutter build apk --release --target-platform android-arm
```

2022-08-14 重构：Flutter > 3.0
2022-09-07 修复加载失败时加载动画不消失的问题，添加注释
